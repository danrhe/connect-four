package nl.ou.connectfour;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import static com.firebase.ui.auth.AuthUI.getApplicationContext;


public class NotificationHelper {

    String CHANNEL_ID = "nl.ou.connectfour.notificationchannelid";
    String description = "Connect Four Default Notification";
    CharSequence channelName = "Connect Four";
    int importance = NotificationManager.IMPORTANCE_HIGH;
    NotificationChannel channel;
    NotificationManager notificationManager;
    Context context;
    Intent intent;
    PendingIntent pendingintent;

    public NotificationHelper(Context context) {
        this.context = context;
    }

    void createChannel() {
        channel = new NotificationChannel(CHANNEL_ID, channelName, importance);
        channel.setDescription(description);
        notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.createNotificationChannel(channel);
    }

    void createNotification(String title, String message, Context context) {
	intent = new Intent(getApplicationContext(), GameGuiActivity.class);
	PendingIntent pendingintent = PendingIntent.getActivity(
		getApplicationContext(),
		0,
		intent,
		PendingIntent.FLAG_UPDATE_CURRENT);
        Notification.Builder builder = new Notification.Builder (context, CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_notifications_black_24dp)
            .setAutoCancel(true)
            .setContentIntent(pendingintent)//context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
            .setContentTitle(title)
            .setContentText(message);
        notificationManager.notify(20, builder.build());
    }

}
