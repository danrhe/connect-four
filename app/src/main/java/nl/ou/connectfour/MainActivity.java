package nl.ou.connectfour;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Objects;

import nl.ou.connectfour.domain.Game;
import nl.ou.connectfour.domain.Player;

import static nl.ou.connectfour.domain.Game.GameType.REMOTE;
import static nl.ou.connectfour.domain.GameManager.createGame;


public class MainActivity extends BaseActivity {

    private static final FirebaseDatabase database = FirebaseDatabase.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Set toolbar allowing to start settings activity
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Set Button REAL GAME
        Button realGameButton = findViewById(R.id.realGameButton);
        realGameButton.setOnClickListener(new RealGameButtonListener());

        // Set Button ACTIVE GAMES
        Button findGameActivityButton = findViewById(R.id.findGameButton);
        findGameActivityButton.setOnClickListener(new FindGameButtonClickListener());

        // Button SINGLE PLAYER
        Button startSinglePlayerButton = findViewById(R.id.startSinglePlayerButton);
        setSinglePlayerListener(startSinglePlayerButton, Player.PlayerType.LOCAL_HUMAN, false);

        // Button START AGAINST ROBOT
        Button startAgainstAppButton = findViewById(R.id.startAgainstRobotButton);
        setSinglePlayerListener(startAgainstAppButton, Player.PlayerType.ROBOT, false);

        // Button ROBOT BEGINS
        Button startRobotBegins = findViewById(R.id.robotStartsButton);
        setSinglePlayerListener(startRobotBegins, Player.PlayerType.ROBOT, true);


    }

    /**
     * Button press adds value event listener to firebase games table
     */
    private class RealGameButtonListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            if (isNotLoggedInYet()) {
                Snackbar.make(view, "You need to log in first", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            } else {
                DatabaseReference refGames = database.getReference().child("games");
                refGames.addListenerForSingleValueEvent(new SingleValueEventListener());
            }
        }
    }


    /**
     * Read the database and search for the first game with status WAITING (and player2 is still empty)
     * ... if found then make yourself player2 and start the game
     * ... if not found then create a new WAITING game, make yourself player1 and leave player2 empty
     */
    private class SingleValueEventListener implements ValueEventListener {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            String userName = getLocalPlayerName();
            String status;
            for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                status = getChild(snapshot, "gamestatus");
                if ("WAITING".equals(status)) {
                    final String p1 = getChild(snapshot, "player1");
                    // p1 contains a value
                    if (p1 == null || "".equals(p1)) throw new AssertionError();
                    final String p2 = getChild(snapshot, "player2");
                    // in a WAITING game player-2 should be empty
                    if (!"".equals(p2)) throw new AssertionError();
                    final String id = snapshot.getKey();
                    Player player1 = new Player(1, p1, Player.PlayerType.REMOTE_PLAYER);
                    Player player2 = new Player(2, userName, Player.PlayerType.LOCAL_HUMAN);
                    createGame(id, player1, player2, REMOTE, Game.GameStatus.ACTIVE);
                    startActivity(new Intent(MainActivity.this, GameGuiActivity.class));
                    return;
                }
            }
            // No waiting games found, create one
            createGame(null,
                    new Player(1, userName, Player.PlayerType.LOCAL_HUMAN),
                    null,
                    REMOTE, Game.GameStatus.WAITING);
            startActivity(new Intent(MainActivity.this, GameGuiActivity.class));
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
            Log.e("Firebase Database error", databaseError.getMessage());
        }

        private String getChild(DataSnapshot dataSnapshot, String key) {
            DataSnapshot element = dataSnapshot.child(key);
            if (element.exists()) {
                return Objects.requireNonNull(element.getValue()).toString();
            }
            return null;
        }

    }

    private class FindGameButtonClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            //        TextView tv = findViewById(R.id.myInfo);
            //        tv.setText(getLocalPlayerName());

            if (isNotLoggedInYet()) {
                Snackbar.make(v, "You need to log in first", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            } else {
                startActivity(new Intent(MainActivity.this, FindGameActivity.class));
            }

        }
    }


    private void setSinglePlayerListener(final Button btn, Player.PlayerType opponent, boolean opponentFirst) {
        String userName = getLocalPlayerName();
        String opponentName = opponent.equals(Player.PlayerType.LOCAL_HUMAN) ? userName + "\'s opponent" : "Robot";
        final Player player1;
        final Player player2;

        if (opponentFirst) {
            player1 = new Player(2, "Robot Player", opponent);
            player2 = new Player(1, userName, Player.PlayerType.LOCAL_HUMAN);

        } else {
            player1 = new Player(1, userName, Player.PlayerType.LOCAL_HUMAN);
            player2 = new Player(2, opponentName, opponent);
        }

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                createGame(null,
                        player1,
                        player2,
                        Game.GameType.SINGLE_PLAYER, Game.GameStatus.ACTIVE);
                startActivity(new Intent(MainActivity.this, GameGuiActivity.class));
            }
        });
    }

    private boolean isNotLoggedInYet() {
        return !PreferenceManager.getDefaultSharedPreferences(MainActivity.this).getBoolean("connect_connect4switch", false);
    }

    private String getLocalPlayerName() {
        return PreferenceManager.getDefaultSharedPreferences(MainActivity.this).getString("display_name", "Player 1");

    }

    // Add menu with settings to view
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    // Action handler for selected option
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            //ActivityCompat.startActivity(this, intent, null);
            //supportFinishAfterTransition();
            TaskStackBuilder builder = TaskStackBuilder.create(this);
            builder.addNextIntentWithParentStack(intent);
            builder.startActivities();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}


