package nl.ou.connectfour;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import nl.ou.connectfour.domain.Game;
import nl.ou.connectfour.domain.GameBoard;

import static nl.ou.connectfour.domain.Game.COLUMNS;


public class GameboardView extends View {

    private Game game;

    private Paint paint;

    /*
        First three constructors are needed in order to integrate with layouts
         */
    public GameboardView(Context context, AttributeSet attrs) {
        super(context, attrs);
        startGame();
    }

    public GameboardView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        startGame();
    }

    public GameboardView(Context context) {
        super(context);
        startGame();
    }

    private void startGame() {
        paint = new Paint();
        game = GameGuiActivity.getGame();
        if (game.getCurrentPlayer().isRobot()) {
            game.playMove(-1);
            invalidate();
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int x = getWidth();
        int y = getHeight();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.BLUE);
        canvas.drawPaint(paint);
        paint.setColor(Color.WHITE);
        paint.setTextSize(60.0f);
        drawGrid(canvas, paint, x, y);
    }

    private void drawGrid(Canvas canvas, Paint paint, int x, int y) {
        GameBoard gameBoard = game.getGameBoard();
        for (int row = 1; row <= Game.ROWS; row++) {
            for (int column = 1; column <= COLUMNS; column++) {
                switch (gameBoard.getFieldValue(column, row)) {
                    case 0:
                        paint.setColor(Color.WHITE);
                        break;
                    case 1:
                        paint.setColor(Color.YELLOW);
                        break;
                    case 2:
                        paint.setColor(Color.RED);
                        break;
                }
                final int RADIUS = x / 18;
                final int LEFTSHIFT = 15;
                final int UPSHIFT = 60;
                canvas.drawCircle((x / COLUMNS) * column - x / LEFTSHIFT,
                        y - (y / (Game.ROWS * 2)) * row + RADIUS - UPSHIFT, RADIUS, paint);
            }
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {

        // Allow a move when
        // - the game is not finished and the player is human player at this device
        if (game.isActive() && game.getCurrentPlayer().isLocalHuman()) {
            float x = e.getX();

            if (e.getAction() == MotionEvent.ACTION_DOWN) {
                game.playMove(getTouchedColumn(x));
                this.invalidate();
            }
            return true;
        } else {
            return false;
        }
    }

    private int getTouchedColumn(float x) {
        int touchedColumn = 0;
        if (x < (getWidth() / COLUMNS)) {
            touchedColumn = 1;
        }
        for (int column = 1; column < COLUMNS; column++) {
            if ((x > ((getWidth() / COLUMNS) * column)) && (x <
                    ((getWidth() / COLUMNS) * (column + 1)))) {
                touchedColumn = column + 1;
            }
        }
        return touchedColumn;
    }


}
