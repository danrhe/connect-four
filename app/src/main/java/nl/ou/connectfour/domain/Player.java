package nl.ou.connectfour.domain;

public class Player {

    public enum PlayerType {
        LOCAL_HUMAN,  // plays on this device
        ROBOT,
        REMOTE_PLAYER
    }

    private final int id;
    private final String name;
    private PlayerType playerType;

    public Player(final int id, final String name, final PlayerType playerType) {
        this.id = id;
        this.name = name;
        this.playerType = playerType;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public boolean isRobot() {
        return playerType == PlayerType.ROBOT;
    }

    public boolean isLocalHuman() {
        return playerType == PlayerType.LOCAL_HUMAN;
    }

    /**
     * Evaluates each column and selects the column with the highest move value
     * value when he plays there himself -plus- 'half of' the value for his opponent
     *
     * @param gameBoard the grid with the stones
     * @return the column wich move has the highest value
     */
    public int findBestMove(GameBoard gameBoard) {
        // getId(), the player is 1 (yellow) or 2 (red). The opponent is the opposit. getId + opponentId = always 3
        int opponentId = 3 - getId();
        // start initially with an extremely bad value and try to find a better move value
        double bestValue = -Double.MAX_VALUE;
        int bestMove = -1;
        // available columns go from 1 upto gameBoard.getColumns() - column 0 isn't used
        for (int column = 1; column <= gameBoard.getColumns(); column++) {
            // is it possible to put another move on this column? Then try it
            if (!gameBoard.columnIsFull(column)) {
                double value = Math.random() +
                        // what is the value when this player puts a stone here as first?
                        moveValue(gameBoard, column, getId()) +
                        // what is the value when his opponent puts a stone here instead?
                        moveValue(gameBoard, column, opponentId) / 2;
                // it's always better to create a row than to avoid the same thing from the opponent,
                // because you're a move ahead on the opponent
                if (bestValue < value) { // a new record has been found
                    bestMove = column;
                    bestValue = value;
                }
            }
        }


        return bestMove;
    }

    /*
     * Return the value for creating a row of this length
     */
    private static final int VALUE_LENGTH_1 = 1;
    private static final int VALUE_LENGTH_2 = 10;
    private static final int VALUE_LENGTH_3 = 100;
    private static final int VALUE_LENGTH_4_OR_MORE = 1000;

    private static int lengthValue(int length) {
        switch (length) {
            case 0:
                return 0; // should be impossible
            case 1:
                return VALUE_LENGTH_1;
            case 2:
                return VALUE_LENGTH_2;
            case 3:
                return VALUE_LENGTH_3;
            default:
                return VALUE_LENGTH_4_OR_MORE;
        }
    }

    /**
     * Count the values of rows-of-stones in all directions together that could be made with this move
     *
     * @param gameBoard    is the actual play area
     * @param column       in which we evaluate a move
     * @param playerNumber (1 = yellow, 2 = red)
     * @return the value of a move for a player in a column
     */
    private static int moveValue(GameBoard gameBoard, int column, int playerNumber) {
        int row = gameBoard.getMovesColumn(column) + 1;

        int moveValue = checkNorthSouth(gameBoard, column, row, playerNumber) +
                checkEastWest(gameBoard, column, row, playerNumber) +
                checkSouthWestNorthEast(gameBoard, column, row, playerNumber) +
                checkSouthEastNorthWest(gameBoard, column, row, playerNumber);
        // if possible - also check what happens is the opponent responds at the same column (one row higher)
        if (row < gameBoard.getRows()) {
            row++;
            int opponentId = 3 - playerNumber; // switches 1 to 2 or back
            moveValue = moveValue - (checkEastWest(gameBoard, column, row, opponentId) +
                    checkSouthWestNorthEast(gameBoard, column, row, opponentId) +
                    checkSouthEastNorthWest(gameBoard, column, row, opponentId)) / 2;
//            it is value for the opponent (thus negative for current player)
//            value is lower (devided by two), because currentplayer is a move ahead on the opponent
        }
        return moveValue;
    }

    /*
     *  counts the length of the vertical row-of-stones for this player - and returns a length-value
     *  when there is no space left for 4 on a row, the value is zero
     */
    private static int checkNorthSouth(GameBoard gameBoard, int column, int row, int playerNumber) {
        int south = row;
        while (south > 1 && gameBoard.getFieldValue(column, south - 1) == playerNumber) {
            south--;
        }
        if (gameBoard.getRows() - south + 1 >= 4) {
            return lengthValue(row - south + 1);
        }
        return 0;
    }

    /*
     *  counts the length of the horizontal row-of-stones for this player - and returns a length-value
     *  also counts the maximum reachable length, if less than 4 reachable, the value is zero
     */
    private static int checkEastWest(GameBoard gameBoard, int column, int row, int playerNumber) {
        int westEnd = column;
        int eastEnd = column;

        while (gameBoard.getFieldValue(westEnd - 1, row) == playerNumber) {
            westEnd--;
        }
        int westMax = westEnd;
        while (gameBoard.getFieldValue(westMax - 1, row) == playerNumber ||
                gameBoard.getFieldValue(westMax - 1, row) == 0) {
            westMax--;
        }

        while (gameBoard.getFieldValue(eastEnd + 1, row) == playerNumber) {
            eastEnd++;
        }
        int eastMax = eastEnd;
        while (gameBoard.getFieldValue(eastMax + 1, row) == playerNumber ||
                gameBoard.getFieldValue(eastMax + 1, row) == 0) {
            eastMax++;
        }

        if (eastMax - westMax + 1 >= 4) {
            return lengthValue(eastEnd - westEnd + 1);
        }
        return 0;
    }

    private static int checkSouthWestNorthEast(GameBoard gameBoard, int column, int row, int playerNumber) {
        int westEnd = column;
        int eastEnd = column;
        int northEnd = row;
        int southEnd = row;

        while (gameBoard.getFieldValue(westEnd - 1, southEnd - 1) == playerNumber) {
            westEnd--;
            southEnd--;
        }
        int westMax = westEnd;
        int southMax = southEnd;
        while (gameBoard.getFieldValue(westMax - 1, southMax - 1) == playerNumber ||
                gameBoard.getFieldValue(westMax - 1, southMax - 1) == 0) {
            westMax--;
            southMax--;
        }

        while (gameBoard.getFieldValue(eastEnd + 1, northEnd + 1) == playerNumber) {
            eastEnd++;
            northEnd++;
        }
        int eastMax = eastEnd;
        int northMax = northEnd;
        while (gameBoard.getFieldValue(eastMax + 1, northMax + 1) == playerNumber ||
                gameBoard.getFieldValue(eastMax + 1, northMax + 1) == 0) {
            eastMax++;
            northMax++;
        }

        if (eastMax - westMax + 1 >= 4) {
            return lengthValue(eastEnd - westEnd + 1);
        }
        return 0;
    }

    private static int checkSouthEastNorthWest(GameBoard gameBoard, int column, int row, int value) {
        int westEnd = column;
        int eastEnd = column;
        int northEnd = row;
        int southEnd = row;

        while (gameBoard.getFieldValue(eastEnd + 1, southEnd - 1) == value) {
            eastEnd++;
            southEnd--;
        }
        int eastMax = eastEnd;
        int southMax = southEnd;
        while (gameBoard.getFieldValue(eastMax + 1, southMax - 1) == value ||
                gameBoard.getFieldValue(eastMax + 1, southMax - 1) == 0) {
            eastMax++;
            southMax--;
        }

        while (gameBoard.getFieldValue(westEnd - 1, northEnd + 1) == value) {
            westEnd--;
            northEnd++;
        }
        int westMax = westEnd;
        int northMax = northEnd;
        while (gameBoard.getFieldValue(westMax - 1, northMax + 1) == value ||
                gameBoard.getFieldValue(westMax - 1, northMax + 1) == 0) {
            westMax--;
            northMax++;
        }

        if (eastMax - westMax + 1 >= 4) {
            return lengthValue(eastEnd - westEnd + 1);
        }
        return 0;
    }

}
