package nl.ou.connectfour.domain;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Game {

    /**
     * GameStatus is used to search in FireBase for people waiting for an opponent
     */
    public enum GameStatus {
        WAITING, // one player has started, but is waiting for the second player to enter
        ACTIVE,
        ENDED
    }

    /**
     * type of game
     */
    public enum GameType {
        SINGLE_PLAYER("Single player game"),
        AGAINST_ROBOT("Game against the robot"),
        REMOTE("Game against someone else via internet");

        private final String description;

        GameType(String description) {
            this.description = description;
        }
    }

    public static final int COLUMNS = 7;
    public static final int ROWS = 6;

    private static final FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference refGame;
    private DatabaseReference refMessage;
    private DatabaseReference refPlayer_2;
    private DatabaseReference refMoves;
    private DatabaseReference refCurrentPlayer; // not really needed - it is derived from the number of turns
    private DatabaseReference refGameStatus;

    private List<Integer> turnList;

    private GameBoard gameBoard;
    private Player player_1, player_2, currentPlayer, opponent;
    private GameType gameType;
    private GameStatus gameStatus;

    private String message;

    public DatabaseReference getRefMessage() {
        return refMessage;
    }

    public DatabaseReference getRefPlayer_2() {
        return refPlayer_2;
    }

    public DatabaseReference getRefMoves() {
        return refMoves;
    }

    public DatabaseReference getRefGameStatus() {
        return refGameStatus;
    }

    public boolean isActive() {
        return gameStatus == GameStatus.ACTIVE;
    }

    public GameBoard getGameBoard() {
        return gameBoard;
    }

    private void setMessage(String message) {
        this.message = message;
        refMessage.setValue(message);
    }

    public Player getPlayer_1() {
        return player_1;
    }

    // player_1 is only set - reference not needed
    private void setPlayer_1(Player player_1) {
        this.player_1 = player_1;
        setDBfield("player1", (player_1 != null) ? player_1.getName() : "");
    }

    public Player getPlayer_2() {
        return player_2;
    }

    // player_2 is listened by GameGuiActivity - keep the reference
    public void setPlayer_2(Player player_2) {
        this.player_2 = player_2;
        refPlayer_2 = refGame.child("player2");
        refPlayer_2.setValue((player_2 != null) ? player_2.getName() : "");
    }

    public boolean iAmPlaying() {
        return player_1.isLocalHuman() || player_2.isLocalHuman();
    }

    public String getMessage() {
        return message;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public GameType getGameType() {
        return gameType;
    }

    public boolean isRemoteGame() {
        return gameType == GameType.REMOTE;
    }

    public GameStatus getGameStatus() {
        return gameStatus;
    }

    public void setGameStatus(GameStatus gameStatus) {
        this.gameStatus = gameStatus;
        refGameStatus.setValue(gameStatus);
    }

    /**
     * When the turnlist is set, the gameboard is fully rebuilt
     * firstly the board is cleared
     * next, all moves are replayed from the turnlist onto the gameboard
     *
     * @param turnList the list of moves, received via FireBase
     */
    public void setTurnList(List<Integer> turnList) {
        this.turnList = turnList;
        gameBoard.clearFields();
        currentPlayer = player_1;
        for (int col : turnList) {
            gameBoard.saveMoveOnBoard(col, currentPlayer.getId());
            changeCurrentPlayer();
        }
    }

    /**
     * Set a database field and forget the reference (e.g. set once - read-only )
     *
     * @param fieldName  fieldname in Firebase
     * @param fieldValue value to store
     */
    private void setDBfield(String fieldName, String fieldValue) {
        DatabaseReference ref = refGame.child(fieldName);
        ref.setValue(fieldValue);
    }

    public Game(String gameId, GameType gameType, Player player_1, Player player_2, GameStatus gameStatus) {
        refGame = database.getReference().child("games").child(gameId);

        setPlayer_1(player_1);
        setPlayer_2(player_2);
        this.gameType = gameType;
        setDBfield("gametype", gameType.toString());

        currentPlayer = player_1;
        opponent = player_2;
        turnList = new ArrayList<>();
        gameBoard = new GameBoard(COLUMNS, ROWS);

        refMessage = refGame.child("message");
        refMoves = refGame.child("moves");

        refGameStatus = refGame.child("gamestatus");
        setGameStatus(gameStatus);

        refCurrentPlayer = refGame.child("currentplayer");
        refCurrentPlayer.setValue(currentPlayer.getName());

        LocalDateTime startTime = LocalDateTime.now();
        setDBfield("starttime", startTime.toString());
    }

    private void changeCurrentPlayer() {
        opponent = currentPlayer;
        if (currentPlayer == player_1) {
            currentPlayer = player_2;
        } else {
            currentPlayer = player_1;
        }
        refCurrentPlayer.setValue(currentPlayer.getName());
    }

    public void playMove(int column) {
        do {
            if (currentPlayer.isRobot()) {
                column = currentPlayer.findBestMove(gameBoard);
            }
            if (!gameBoard.isMoveValid(column)) {
                setMessage("Invalid move");
                return;
            }
            gameBoard.saveMoveOnBoard(column, currentPlayer.getId());
            turnList.add(column);
            if (gameBoard.fourAreConnected(column, currentPlayer.getId())) {
                finishGame(currentPlayer.getName() + " has won the game");
                setDBfield("winner", currentPlayer.getName());
                return;
            }
            int MAX_TURNS = COLUMNS * ROWS;
            if (turnList.size() >= MAX_TURNS) {
                finishGame("The game has ended in draw");
                return;
            }

            changeCurrentPlayer();

        } while (isActive() && currentPlayer.isRobot());
        refMoves.setValue(turnList);
    }

    /**
     * the current player looses by leaving the game - resignation
     */
    public void escape() {
        finishGame(currentPlayer.getName() + " has exited the game and lost");
        setDBfield("winner", opponent.getName());
    }

    private void finishGame(String message) {
        setMessage(message);
        setGameStatus(GameStatus.ENDED);
        refMoves.setValue(turnList);
    }

}
