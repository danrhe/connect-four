package nl.ou.connectfour.domain;

import com.google.firebase.database.FirebaseDatabase;

public class GameManager {
    private static final FirebaseDatabase database = FirebaseDatabase.getInstance();
    private static Game game;

    public static Game getGame() {
        return game;
    }

    /**
     * Create a game locally and possibly in firebase too
     *
     * @param gameId   - gameId of the game that existst in firebase (get a waiting or active game)
     *                 - or gameId = 'null' to create a new one in firebase
     * @param player1  - plays yellow
     * @param player2  - plays red
     * @param gameType - Single player game, Game against the robot, Game against someone else via internet
     * @param gameStatus - WAITING or ACTIVE (ENDED should not be used here)
     */
    public static void createGame(String gameId, Player player1, Player player2, Game.GameType gameType, Game.GameStatus gameStatus) {
        String id = gameId;
        if (id == null) {
            id = database.getReference().child("games").push().getKey();
        }
        if ((gameStatus != Game.GameStatus.WAITING && gameStatus != Game.GameStatus.ACTIVE))
            throw new AssertionError();
        game = new Game(id, gameType, player1, player2, gameStatus);
    }

    /**
     * Start a new game, same type, same players
     * Via GUI only allowed for local games = single-player and against robot
     */
    public static void resetGame() {
        assert game != null;
        String id = database.getReference().child("games").push().getKey();
        game = new Game(id, game.getGameType(), game.getPlayer_1(), game.getPlayer_2(), Game.GameStatus.ACTIVE);
    }
 }
