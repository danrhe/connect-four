package nl.ou.connectfour.domain;

public class GameBoard {
    private final int columns;
    private final int rows;
    private final int[][] fields;

    public GameBoard(int columns, int rows) {
        this.columns = columns;
        this.rows = rows;
        this.fields = new int[columns + 1][rows + 1];
        clearFields();
    }

    public void clearFields() {
        for (int col = 1; col <= getColumns(); col++) {
            for (int row = 1; row <= getRows(); row++) {
                fields[col][row] = 0;
            }
        }
    }

    public int getColumns() {
        return columns;
    }

    public int getRows() {
        return rows;
    }

    public int getFieldValue(int column, int row) {
        if (column <= 0 || column > this.columns || row <= 0 || row > this.rows) {
            return -1;
        }

        return fields[column][row];
    }

    public int[] getColumn(int column) {

        return fields[column];

    }

    public boolean columnIsFull(int column) {
        return (fields[column][rows] != 0);
    }

    public int getMovesColumn(int column) {
        if (column <= 0 || column > this.columns) {
            return -1;
        }

        int moves = 0;
        int index = 1;
        int value = getFieldValue(column, index);
        while (index <= rows && value > 0) {
            index++;
            moves++;
            value = getFieldValue(column, index);
        }
        return moves;
    }

    public boolean saveMoveOnBoard(int column, int playerId) {
        int row = getMovesColumn(column) + 1;
        if (column <= 0 || column > this.columns || row <= 0 || row > this.rows) {
            return false;
        }

        fields[column][row] = playerId;
        return true;
    }

    public boolean isMoveValid(int column) {
        // non-existing column
        if (column < 0 || column > columns) {
            return false;
        }
        // column is not full already?
        return (getMovesColumn(column) < rows);
    }


    public boolean fourAreConnected(int column, int playerId) {
        int row = getMovesColumn(column);

        return checkNorthSouth(column, row, playerId) ||
                checkEastWest(column, row, playerId) ||
                checkSouthEastNorthWest(column, row, playerId) ||
                checkSouthWestNorthEast(column, row, playerId);

    }

    /**
     * Determines whether the gameboard has an uninterrupted vertical sequence of four or more times
     * the same value starting at field with indexes [column,row]
     *
     * @param column Column index.
     * @param row    Row index.
     * @param value  Value to check for in matrix.
     * @return true if 4 or more same values are found in a row.
     */
    private boolean checkNorthSouth(int column, int row, int value) {
        int south = row;
        int north = row;

        while (getFieldValue(column, south - 1) == value) {
            south--;
        }

        while (getFieldValue(column, north + 1) == value) {
            north++;
        }

        return (north - (south - 1) >= 4);
    }

    /**
     * Determines whether the gameboard has an uninterrupted horizontal sequence of four or more times
     * the same value starting at field with indexes [column,row]
     *
     * @param column Column index.
     * @param row    Row index.
     * @param value  Value to check for in matrix.
     * @return true if 4 or more same values are found in a row.
     */
    private boolean checkEastWest(int column, int row, int value) {
        int westEnd = column;
        int eastEnd = column;

        while (getFieldValue(westEnd - 1, row) == value) {
            westEnd--;
        }

        while (getFieldValue(eastEnd + 1, row) == value) {
            eastEnd++;
        }

        return (eastEnd - (westEnd - 1) >= 4);
    }

    /**
     * Determines whether the gameboard has an uninterrupted up/right diagonal sequence of four or more times
     * the same value with indexes [column,row] as starting point.
     * <p>
     * NB Method searches fields 1) down/left and 2) up/right from starting point for same value as one provided.
     * As value deviates last indexes are taken to determine length of sequence.
     *
     * @param column Column index.
     * @param row    Row index.
     * @param value  Value to check for in matrix.
     * @return true if 4 or more same values are found in a row.
     */
    private boolean checkSouthWestNorthEast(int column, int row, int value) {
        int westEnd = column;
        int eastEnd = column;
        int northEnd = row;
        int southEnd = row;

        while (getFieldValue(westEnd - 1, southEnd - 1) == value) {
            westEnd--;
            southEnd--;
        }

        while (getFieldValue(eastEnd + 1, northEnd + 1) == value) {
            eastEnd++;
            northEnd++;
        }

        return (eastEnd - (westEnd - 1) >= 4);
    }

    /**
     * Determines whether the gameboard has an uninterrupted up/left diagonal sequence of four or more times
     * the same value with indexes [column,row] as starting point.
     * <p>
     * NB Method searches fields 1) down/right and 2) up/left from starting point for same value as one provided.
     * As value deviates, last indexes are taken to determine length of sequence.
     *
     * @param column Column index.
     * @param row    Row index.
     * @param value  Value to check for in matrix.
     * @return true if 4 or more same values are found in a row.
     */
    private boolean checkSouthEastNorthWest(int column, int row, int value) {
        int westEnd = column;
        int eastEnd = column;
        int northEnd = row;
        int southEnd = row;

        while (getFieldValue(eastEnd + 1, southEnd - 1) == value) {
            eastEnd++;
            southEnd--;
        }

        while (getFieldValue(westEnd - 1, northEnd + 1) == value) {
            westEnd--;
            northEnd++;
        }

        return (eastEnd - (westEnd - 1) >= 4);
    }
}
