package nl.ou.connectfour;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Objects;

import nl.ou.connectfour.domain.Game;
import nl.ou.connectfour.domain.GameManager;
import nl.ou.connectfour.domain.Player;

public class FindGameActivity extends AppCompatActivity {
    private static final FirebaseDatabase database = FirebaseDatabase.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_game);

        DatabaseReference refGames = database.getReference().child("games");
        refGames.addValueEventListener(gameListener);
    }

    /**
     * Listens to a games list and ...
     */
    private final ValueEventListener gameListener = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            int count = 0;
            LinearLayout foundGamesLayout = findViewById(R.id.foundlayout);
            foundGamesLayout.setOrientation(LinearLayout.VERTICAL);
            foundGamesLayout.removeAllViews(); // clear buttons from previous call
            for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                String status = getChild(snapshot, "gamestatus");
                if ((Game.GameStatus.ACTIVE.name()).equals(status)) {
                    final String p1 = getChild(snapshot, "player1");
                    final String p2 = getChild(snapshot, "player2");
                    final String id = snapshot.getKey();
                    Button btn = new Button(foundGamesLayout.getContext());
                    btn.setText(p1 + "-" + p2);
                    btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            GameManager.createGame(id,
                                    new Player(1, p1, Player.PlayerType.REMOTE_PLAYER),
                                    new Player(2, p2, Player.PlayerType.REMOTE_PLAYER),
                                    Game.GameType.REMOTE, Game.GameStatus.ACTIVE);
                            startActivity(new Intent(FindGameActivity.this, GameGuiActivity.class));
                        }
                    });
                    foundGamesLayout.addView(btn);
                    count++;
                }
            }
            TextView textView = new TextView(foundGamesLayout.getContext());
            textView.setText(count + " active games found");
            foundGamesLayout.addView(textView);
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
            TextView textView = findViewById(R.id.textTurn);
            textView.setText("Failed to read games.");
        }
    };

    private String getChild(DataSnapshot dataSnapshot, String key) {
        DataSnapshot element = dataSnapshot.child(key);
        if (element.exists()) {
            return Objects.requireNonNull(element.getValue()).toString();
        }
        return null;
    }
}
