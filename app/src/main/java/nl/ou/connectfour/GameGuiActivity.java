package nl.ou.connectfour;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import nl.ou.connectfour.domain.Game;
import nl.ou.connectfour.domain.GameManager;
import nl.ou.connectfour.domain.Player;

import static android.widget.Toast.LENGTH_LONG;

public class GameGuiActivity extends Activity {

    // todo work towards removal of this ugly, temporary static attribute and getter
    static private Game game;

    NotificationHelper channelHelper;
    Context context;
    boolean showNotification;

    static public Game getGame() {
        return game;
    }

    private GameboardView gameboardView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.SplashTheme);

        initGame();

        setContentView(R.layout.activity_single_player);
        gameboardView = findViewById(R.id.gameboardview);

        context = this;
        channelHelper = new NotificationHelper(context);
        channelHelper.createChannel();

        // only allow restart in a fully local game
        Button restartButton = findViewById(R.id.restartgame);
        if (game.isRemoteGame()) {
            restartButton.setVisibility(View.INVISIBLE);
        } else {
            restartButton.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        showNotification = true;
        /*
        if (game.iAmPlaying()) {
            game.escape(); // exiting the game means resign
        }
        */
    }

    @Override
    protected void onResume() {
        super.onResume();
        showNotification = false;
    }

    private void initGame() {
        game = GameManager.getGame(); // the game has already been created by calling activity
        assert game != null;

//        try{
            DatabaseReference refMessage = game.getRefMessage();
            refMessage.addValueEventListener(messageListener);

            DatabaseReference refMoves = game.getRefMoves();
            refMoves.addValueEventListener(moveListener);

        DatabaseReference refGameStatus = game.getRefGameStatus();
        refGameStatus.addValueEventListener(gameStatusListener);

            DatabaseReference refPlayer_2 = game.getRefPlayer_2();
            refPlayer_2.addValueEventListener(player2Listener);


//        } catch (NullPointerException e){
//
//            Log.e("Firebase","Unable to make connection with Firebase database");
//        }

    }

    /**
     * Listens to a games' turn list and executes changes on display
     */
    private final ValueEventListener moveListener = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            List<Integer> turns = new ArrayList<>();
            for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                Long turn = (Long) snapshot.getValue();
                assert turn != null;
                turns.add(turn.intValue());
            }
            if (showNotification) {
                channelHelper.createNotification("Connect Four", "it is your turn!", context);
                Toast.makeText(context, "Connect Four: it is your turn!", LENGTH_LONG).show();
                showNotification = false;
            }
            game.setTurnList(turns);
            TextView textView = findViewById(R.id.textTurn);
            textView.setText(turns.size() + " turns ");
            setNextPlayerText();
            gameboardView.invalidate();
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
            setMsg("Failed to read turns");
        }
    };

    public void clickRestartSinglePlayer(View view) {
        GameManager.resetGame();
        initGame();

        setContentView(R.layout.activity_single_player);
    }

    private final ValueEventListener messageListener = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            String message = dataSnapshot.getValue(String.class);
            setMsg(message);
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
            setMsg("Failed to read message");
        }
    };

    private ValueEventListener gameStatusListener = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            String status = dataSnapshot.getValue(String.class);
            try {
                game.setGameStatus(Game.GameStatus.valueOf(status));
                setNextPlayerText();
            } catch (IllegalArgumentException e) {
                setMsg("Error reading game status");
            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
            setMsg("Failed to read game status");
        }
    };

    private ValueEventListener player2Listener = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            String player_2 = dataSnapshot.getValue(String.class);
            if (player_2 != null && !player_2.isEmpty() && game.getPlayer_2() == null) {
                // player 2 has entered the game - now create it
                game.setPlayer_2(new Player(2, player_2, Player.PlayerType.REMOTE_PLAYER));
            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
            setMsg("Failed to read player 2");
        }
    };

    /**
     * Displays a next-player text depending on the game status
     */
    private void setNextPlayerText() {
        String nextPlayerMsg = "";
        switch (game.getGameStatus()) {
            case WAITING:
                nextPlayerMsg = getString(R.string.wait_for_opponent);
                break;
            case ACTIVE:
                nextPlayerMsg = game.getCurrentPlayer().getName() + " is on turn" +
                        (!game.getCurrentPlayer().isLocalHuman() ? " (please wait)" :"");
                Player p = game.getCurrentPlayer();
                nextPlayerMsg = p.getName() + " is on turn" +
                        (!p.isLocalHuman() ? " (please wait)" : " (you)");
                break;
            case ENDED:
                nextPlayerMsg = getString(R.string.game_ended);
                break;
        }
        TextView nextPlayerView = findViewById(R.id.textCurrentPlayer);
        nextPlayerView.setText(nextPlayerMsg);
        gameboardView.invalidate();
    }

    private void setMsg(String msg) {
        TextView textView = findViewById(R.id.textMessage);
        textView.setText(msg);
        gameboardView.invalidate();
    }
}
