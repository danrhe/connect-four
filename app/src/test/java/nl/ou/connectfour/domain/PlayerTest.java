package nl.ou.connectfour.domain;

import org.junit.Test;

import static org.junit.Assert.*;

public class PlayerTest {
    @Test
    public void playerTypeTest() {
        Player hp = new Player(1, "a human player", Player.PlayerType.LOCAL_HUMAN);
        assertTrue(hp.isLocalHuman());
        Player rp = new Player(1, "a robot player", Player.PlayerType.ROBOT);
        assertTrue(rp.isRobot());
    }


    @Test
    public void findBestMoveTest_Simple() {
        GameBoard gb = new GameBoard(2, 6);
        fillColumn(gb, 1, new int[]{1, 1, 2, 2, 2}); // column 1 contains 1-1-2-2-2-0
        fillColumn(gb, 2, new int[]{2, 2, 1, 1, 1}); // column 2 contains 2-2-1-1-1-0

        Player rp1 = new Player(1, "a robot 1", Player.PlayerType.ROBOT);
        Player rp2 = new Player(2, "a robot 2", Player.PlayerType.ROBOT);
        // A robot player should prefer to make 4 - instead of preventing 4 from the opponent
        assertEquals(2, rp1.findBestMove(gb));
        assertEquals(1, rp2.findBestMove(gb));
    }

    @Test
    public void findBestMoveTest_Advanced() {
        GameBoard gb = new GameBoard(7, 6);
        fillColumn(gb, 2, new int[]{2, 2, 2, 1, 2});
        fillColumn(gb, 3, new int[]{1, 2, 1, 1, 2});
        fillColumn(gb, 4, new int[]{2, 2});
        fillColumn(gb, 5, new int[]{1, 1, 2, 1});
        fillColumn(gb, 6, new int[]{2, 1, 2, 2, 1});

        Player rp1 = new Player(1, "a robot 1", Player.PlayerType.ROBOT);
        Player rp2 = new Player(2, "a robot 2", Player.PlayerType.ROBOT);
        assertEquals(5, rp1.findBestMove(gb));

        // for Player 2 column 4 seems attractive, but he better plays something else ...
        // ... his opponent Player 1 will make 4 on a row on top of column 4
        assertNotEquals(4, rp2.findBestMove(gb));
    }

    private void fillColumn(GameBoard gameBoard, int column, int[] moves) {
        for (int move : moves) {
            gameBoard.saveMoveOnBoard(column, move);
        }
    }
}