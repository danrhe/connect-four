package nl.ou.connectfour;

import org.junit.Test;

import java.util.Arrays;

import nl.ou.connectfour.domain.GameBoard;

import static org.junit.Assert.*;

public class GameBoardTest {


    @Test
    public void GameBoardTestInValidFields() {

        GameBoard gb = new GameBoard(2, 1);

        assertFalse(gb.saveMoveOnBoard(3, 1));

        assertFalse(gb.saveMoveOnBoard(-1, 1));

        gb.saveMoveOnBoard(1, 2);
        assertEquals(2, gb.getFieldValue(1, 1));

        assertEquals(-1, gb.getFieldValue(1, 2));

        assertEquals(0, gb.getMovesColumn(2));
        assertEquals(1, gb.getMovesColumn(1));
        int[] col = gb.getColumn(1);
        System.out.println(Arrays.toString(col));

    }


}