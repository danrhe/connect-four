package nl.ou.connectfour;


import android.support.test.espresso.UiController;
import android.support.test.espresso.ViewAction;
import android.support.test.espresso.action.MotionEvents;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import org.hamcrest.Matcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static androidx.test.espresso.matcher.ViewMatchers.isAssignableFrom;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class SinglePlayerActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void showCorrectMessagekEndGame(){
        // Create a new single player game from MainActivity
        onView(withId(R.id.startSinglePlayerButton)).perform(click());
        // Press alternating button until a row of 4 has been formed
        onView(withId(R.id.gameboardview))
                .perform(touchDownAndUp(200,200));
        onView(withId(R.id.gameboardview))
                .perform(touchDownAndUp(800,200));
        onView(withId(R.id.gameboardview))
                .perform(touchDownAndUp(200,200));
        onView(withId(R.id.gameboardview))
                .perform(touchDownAndUp(800,200));
        onView(withId(R.id.gameboardview))
                .perform(touchDownAndUp(200,200));
        onView(withId(R.id.gameboardview))
                .perform(touchDownAndUp(800,200));
        onView(withId(R.id.gameboardview))
                .perform(touchDownAndUp(200,200));

        // Debug
        // String check = getText(withId(R.id.textCurrentPlayer));

        // Check that correct feedback is sent.
        onView(withId(R.id.textCurrentPlayer))
                .check(matches(withText("The game has ended")));
    }


    /**
     * Simulation of a touch event
     * @param x
     * @param y
     * @return
     */
    public static ViewAction touchDownAndUp(final float x, final float y) {
        return new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return isDisplayed();
            }

            @Override
            public String getDescription() {
                return "Send touch events.";
            }

            @Override
            public void perform(UiController uiController, final View view) {
                // Get view absolute position
                int[] location = new int[2];
                view.getLocationOnScreen(location);

                // Offset coordinates by view position
                float[] coordinates = new float[] { x + location[0], y + location[1] };
                float[] precision = new float[] { 1f, 1f };

                // Send down event, hold, and release
                MotionEvent down = MotionEvents.sendDown(uiController, coordinates, precision).down;
                uiController.loopMainThreadForAtLeast(200);
                MotionEvents.sendUp(uiController, down, coordinates);
            }
        };
    }

    /**
     * Get text of a TextView
     * Mainly used to get debug info
     * @param matcher
     * @return
     */
    String getText(final Matcher<View> matcher) {
        final String[] stringHolder = { null };
        onView(matcher).perform(new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return isAssignableFrom(TextView.class);
            }

            @Override
            public String getDescription() {
                return "getting text from a TextView";
            }

            @Override
            public void perform(UiController uiController, View view) {
                TextView tv = (TextView)view; //Save, because of check in getConstraints()
                stringHolder[0] = tv.getText().toString();
            }
        });
        return stringHolder[0];
    }

}
