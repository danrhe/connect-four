# Connect Four
Our mission is to create an Android app of the game Connect Four

## Learning Goals
- Use Android Studio
- Create a reactive GUI
- Create background processes
- Reach realtime communication between two (remote, identical) apps
- Create and use a REST API
- Create and use a local database
- Make an app available in the Play Store and online (Instant App)
- Optimize for size and startuptime
- Develop according to the Twelve Factor App metholodogy?
- Develop for Wear OS?
- Use blockchain technology?
- AndroidX
- Android App Bundle
- Dynamic Delivery

## Requirements
- Communication with a server
- Datatransfer between apps
- Use of external data
- Support for offline use
- API level <= 21

## Minimum Viable Product
The minimum viable product of our app lets a single user play a game,
playing both sides. We should be able to get this MVP up and running on
short notice to get a feeling where we are heading to.

The app does ..
- let the user play a game, playing both sides
- have (very) simple graphics as a Proof of Concept (Poc)

The app does not ..
- allow to resign the game
- have the possibility to register a user
- have the possibility to play against another user
- have a connection to to the server or other apps
- store any data
- include a server
- enable to go back to the previous move

## Extensions
- store the moves of the game
- include a server
- has an option to resign
- receive a move from the database (like playing yourself)
- play on more than one smartphone
- register a user
- invite another user & accept/decline invitation
- play against another user

## Way of Working
- start with the OU app template
- next create the 'play a move' activity

## Roadmap

## Documents
- Business Model Canvas: https://bmfiddle.com/f/#/zG6d2

## Activities
> Activity:
>     A single screen in an application, with supporting Java code,
> derived from the Activity class. Most commonly, an activity is visibly
> represented by a full screen window that can receive and handle UI
> events and perform complex tasks, because of the Window it uses to
> render its window. Though an Activity is typically full screen, it can
> also be floating or transparent.

- startup
- register
  - email
  - password
  - pin
- games  (similar to signal app)
  - active games
  - new game
- new game (based on addressbook, similar to signal app)
- statistics visible for user
  - game time
  - average game time
  - average amount of games per day
  - install date
  - version
  - high score
  - number of installs
  - number of active players (define 'active')
- settings
  - colors
  - notifications
- about
  - about game
  - about us
  - about app

- statistics visible for developers
  - installs
  - versions
  - total game time
  - average game time
  - total amount of games per day
  - average amount of games per day
  - android versions
  - screen resolutions
  - portrait/landscape preference

## Todo
- Fill in the above
- Roadmap
- Create impression of GUI
- Determine possible source of open data
- Create CI/CD pipeline

## Pitfalls
- Done is better than perfect
- No focus

## Notes
- Register with email and password
- Unlock app with four digit pin
- Automatically lookup other players based on addressbook
- Verification of user actions ('moves') on server

## Sprints
### ~~Sprint 201822 (deadline: 2018-06-05 15:00)~~
- Create team
- Determine app
- Add your name to `namen.txt` in Playground
- Create project in gitlab
- Create Business Model Canvas


### Sprint 201824 (deadline: 2018-06-17 23:59)
- Create a gitlab issue for each user story
- Create subtasks for (within) the user stories
- Create a data model

### Sprint 201826
### Sprint 201828
### Sprint 201830
### Sprint 201822
### Sprint 201834
### Sprint 201836
### Sprint 201838
### Sprint 201840
### Sprint 201842
### Sprint 201844
### Sprint 201846
### Sprint 201848
### Sprint 201850
### Sprint 201852
- register with email and password
- unlock app with four digit pin
- automatically lookup other players based on addressbook

## Meetings
### 2018-06-05
- Next sprint is 201824
- Screen orientation will be forced to portrait for MVP
- 7 columns, 6 rows (same as plastic game)
- Whole column can be touched to select
- First step will be, dividing screen in columns, and being able to
  actually select that column
- React on outcome (win or draw)
- Activities (3)
  - Startup
  - Game
  - Outcome
- take a decision: should a game be played in one go (e.g. some minutes per move),
  or should a player respond in 24 hours (with a number of spare holidays)? - like correspondence chess
